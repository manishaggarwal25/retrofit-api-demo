package com.firestudio.retrofitapiexample.rest

import com.firestudio.retrofitapiexample.Modal.Weather
import com.firestudio.retrofitapiexample.Modal.WeatherResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

 @GET("/data/2.5/weather?")
 fun getWeatherReport(@Query("q")cityname: String,@Query("appid")appid:String):Call<WeatherResponse>



 @GET("")
 fun getData(@Query("q")cityname: String):Call<WeatherResponse>
}