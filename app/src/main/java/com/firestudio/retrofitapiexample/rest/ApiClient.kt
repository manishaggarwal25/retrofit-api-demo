package com.firestudio.retrofitapiexample.rest

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient {

   val BASE_URL="https://samples.openweathermap.org/"
    var retrofit:Retrofit?=null


fun getClient():Retrofit?{
    if(retrofit==null){
         retrofit = Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build()
    }
return retrofit }

}

