package com.firestudio.retrofitapiexample.Modal

data class Wind(
    val deg: Int,
    val speed: Double
)