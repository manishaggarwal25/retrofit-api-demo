package com.firestudio.retrofitapiexample.Modal

data class Coord(
    val lat: Double,
    val lon: Double
)