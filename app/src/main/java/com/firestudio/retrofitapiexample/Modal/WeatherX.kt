package com.firestudio.retrofitapiexample.Modal

data class WeatherX(
    val description: String,
    val icon: String,
    val id: Int,
    val main: String
)