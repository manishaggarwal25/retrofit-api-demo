package com.firestudio.retrofitapiexample

import android.content.Context
import android.widget.Toast

class UsefulData {

fun Context.toast(message:String){
    Toast.makeText(applicationContext,message,Toast.LENGTH_SHORT).show()
}

}