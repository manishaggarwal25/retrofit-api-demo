package com.firestudio.retrofitapiexample

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.firestudio.retrofitapiexample.Modal.Weather
import com.firestudio.retrofitapiexample.Modal.WeatherResponse
import com.firestudio.retrofitapiexample.rest.ApiInterface
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MainActivity : AppCompatActivity() {
      val BASE_URL = "https://samples.openweathermap.org/"
       override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        londonbtn.setOnClickListener {

            val logging = HttpLoggingInterceptor()
            logging.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client = OkHttpClient.Builder()
                .addInterceptor(logging)
                .build()


            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
             val apiService: ApiInterface = retrofit.create(ApiInterface::class.java)

            val call=apiService.getWeatherReport("london","b6907d289e10d714a6e88b30761fae22")
            //  val call: Call<Weather> = apiService.getWeatherReport("london", "b6907d289e10d714a6e88b30761fae22")
              call.enqueue(object : Callback<WeatherResponse> {
                override fun onResponse(call: Call<WeatherResponse>, response: Response<WeatherResponse>) {
             //       if (response.code() == 200) {
                 Log.d("Tag ","${response.code()}")
                 var weather=response.body()
                    var tempo =weather?.main?.temp
                    var sunrisevalue =weather?.sys?.sunrise
                    Log.d("Sunrise","$sunrisevalue")
                    weather?.weather?.forEach {
                        Log.e("weather info",""+it.main)
                    }


Log.d("TagOne","$tempo")

                Toast.makeText(applicationContext,"Hello",Toast.LENGTH_SHORT).show()
               //     }
                }
                override fun onFailure(call: Call<WeatherResponse>, t: Throwable) {
                 Log.e("Error","$t")
                Toast.makeText(applicationContext,"Failed",Toast.LENGTH_SHORT).show()
                }
                })


    }

}
}
